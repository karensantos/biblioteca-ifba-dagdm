# Biblioteca IFBA  DAGDM - 2021.1

O objetivo aqui é contribuir abertamente, seja indicando links de drives, cursos, vídeos, artigos ou quaisquer outras contibuições! 

- Repositório compartilhado de materiais: [google drive](https://drive.google.com/drive/folders/1PzG2JI3JWeBCdH3f62Icu0kr5qFlNW4C?usp=sharing)

## Indicações

GDD: 
- Fundamentos para criação de GDD | Alura: [link para leitura](https://www.alura.com.br/conteudo/game-design)
- GDD | RPG Maker: [link youtube](https://youtu.be/1g9I-GN0H0k)
- Passos para fazer um GDD  | Fábrica de Jogos: [link youtube](https://youtu.be/JdMlfvSKaVg)
- Modelo de GDD: [link google docs](https://docs.google.com/document/d/1axeeBWp683LPU8gCBQQqmquHMYHuG3uhNTN0LjSJBKk/edit)


